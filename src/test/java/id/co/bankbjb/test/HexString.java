/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.test;

import java.io.UnsupportedEncodingException;

/**
 *
 * @author LENOVO
 */
public class HexString {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String hex = "e6";
        int decimal = Integer.parseInt(hex, 16);
        System.out.println((char) decimal);
        
        byte[] bytes = hexStringToByteArray(hex);
        System.out.println(new String(bytes, "UTF-8"));
        System.out.println(new String(bytes, "ISO-8859-1"));
    }

    /* s must be an even-length string. */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
