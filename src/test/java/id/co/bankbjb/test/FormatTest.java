/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.test;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 * @author LENOVO
 */
public class FormatTest {
    
    public static void main(String[] args) {
        DecimalFormat df = new DecimalFormat("#0.0000");
        BigDecimal bigD = new BigDecimal("125000.012");
        
        String result = df.format(bigD);
        System.out.println(result);
        
        String resultNegate = df.format(bigD.negate());
        System.out.println(resultNegate);
        
        int counter = 0;
        for (int i = 0; i < 10; i++) {
            System.out.println(String.format("%026d", counter));
            
            counter++;
        }
    }
}
