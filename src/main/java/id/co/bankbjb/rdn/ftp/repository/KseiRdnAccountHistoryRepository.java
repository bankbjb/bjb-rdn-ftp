/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.repository;

import id.co.bankbjb.rdn.ftp.model.KseiRdnAccountHistory;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author LENOVO
 */
@Repository
public interface KseiRdnAccountHistoryRepository extends JpaRepository<KseiRdnAccountHistory, String> {
    
    @Query("SELECT h FROM KseiRdnAccountHistory h WHERE h.activityDate =:activityDate")
    public List<KseiRdnAccountHistory> findYesterdayHistory(@Param("activityDate") Date activityDate);
}
