/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.controller;

import id.co.bankbjb.rdn.ftp.model.KseiRdnAccountHistory;
import id.co.bankbjb.rdn.ftp.model.KseiRdnBalance;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author LENOVO
 */
public class FileHandler {

    private static final Logger logger = LoggerFactory.getLogger(FileHandler.class);

    private static final String decimalFormatStr = "#0.0000";

    public JSONObject createFileReconActStmnt2(JSONObject request, Path filePath) {
        JSONObject jsonResponse = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");

        jsonResponse.put("filename", filePath.getFileName().toString());

        try {
            FileWriter fileWriter = new FileWriter(filePath.toFile(), true);

            JSONObject mpi = request.getJSONObject("MPI");
            JSONObject mpo = request.getJSONObject("MPO");

            PrintWriter printWriter = new PrintWriter(fileWriter);

            SimpleDateFormat sdf2 = new SimpleDateFormat("MMddHHmmssSSS");
            DecimalFormat df = new DecimalFormat("#0.0000");

            int seq = 0;
            Iterator<String> keys = mpo.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (!mpo.getJSONObject(key).has("ZHBALD")) {
                    String reffNum = "bjbtest" + sdf2.format(new Date());
                    seq++;

                    JSONObject mutasi = mpo.getJSONObject(key);
                    BigDecimal closeBal = (mutasi.getString("ZHRBAL").contains("CR"))
                            ? new BigDecimal(mutasi.getString("ZHRBAL").replaceAll("[^\\d]", ""))
                            : new BigDecimal(mutasi.getString("ZHRBAL").replaceAll("[^\\d]", "")).negate();
                    BigDecimal cashVal = (mutasi.getString("ZHAMCR").isEmpty())
                            ? new BigDecimal(mutasi.getString("ZHAMDR").replaceAll("[^\\d]", ""))
                            : new BigDecimal(mutasi.getString("ZHAMCR").replaceAll("[^\\d]", ""));
                    BigDecimal openBal = (mutasi.getString("ZHAMCR").isEmpty())
                            ? closeBal.add(cashVal)
                            : closeBal.subtract(cashVal);

                    String debetcredit = (mutasi.getString("ZHAMCR").isEmpty()) ? "D" : "C";
                    String trxDesc = mutasi.getString("ZHNR") + mutasi.getString("ZHNAR2") + mutasi.getString("ZHNAR3") + mutasi.getString("ZHNAR4");

                    printWriter.println(reffNum
                            + "|" + seq
                            + "|" + mpi.getString("ZLEAN")
                            + "|" + "IDR"
                            + "|" + sdf.format(sdf1.parse(mutasi.getString("ZHSDAT")))
                            + "|" + df.format(openBal)
                            + "|" + mutasi.getString("ZHPSQ") + "|"
                            + "|" + debetcredit
                            + "|" + df.format(cashVal)
                            + "|" + trxDesc.replaceAll("\\|", " ")
                            + "|" + df.format(closeBal)
                            + "|");
                }
            }

            printWriter.close();
        } catch (IOException | ParseException ex) {
            logger.error(ex.getMessage());

            jsonResponse.put("status", false);

            return jsonResponse;
        }

        jsonResponse.put("status", true);

        return jsonResponse;
    }

    public JSONObject createFileReconActStmnt(JSONObject request, String localDir) {
        JSONObject jsonResponse = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");
        String fileName = "RecActStmt_BJB03_" + sdf.format(new Date()) + "_" + request.getString("SEQ") + ".fsp";

        jsonResponse.put("filename", fileName);

        try {
            FileWriter fileWriter = new FileWriter(localDir + fileName);

            JSONObject mpi = request.getJSONObject("MPI");
            JSONObject mpo = request.getJSONObject("MPO");

            PrintWriter printWriter = new PrintWriter(fileWriter);

            SimpleDateFormat sdf2 = new SimpleDateFormat("MMddHHmmssSSS");
            DecimalFormat df = new DecimalFormat("#0.0000");

            int seq = 0;
            Iterator<String> keys = mpo.keys();

            while (keys.hasNext()) {
                String key = keys.next();
                if (!mpo.getJSONObject(key).has("ZHBALD")) {
                    String reffNum = "bjbtest" + sdf2.format(new Date());
                    seq++;

                    JSONObject mutasi = mpo.getJSONObject(key);
                    BigDecimal closeBal = (mutasi.getString("ZHRBAL").contains("CR"))
                            ? new BigDecimal(mutasi.getString("ZHRBAL").replaceAll("[^\\d]", ""))
                            : new BigDecimal(mutasi.getString("ZHRBAL").replaceAll("[^\\d]", "")).negate();
                    BigDecimal cashVal = (mutasi.getString("ZHAMCR").isEmpty())
                            ? new BigDecimal(mutasi.getString("ZHAMDR").replaceAll("[^\\d]", ""))
                            : new BigDecimal(mutasi.getString("ZHAMCR").replaceAll("[^\\d]", ""));
                    BigDecimal openBal = (mutasi.getString("ZHAMCR").isEmpty())
                            ? closeBal.add(cashVal)
                            : closeBal.subtract(cashVal);

                    String debetcredit = (mutasi.getString("ZHAMCR").isEmpty()) ? "D" : "C";

                    printWriter.println(reffNum
                            + "|" + seq
                            + "|" + mpi.getString("ZLEAN")
                            + "|" + "IDR"
                            + "|" + sdf.format(sdf1.parse(mutasi.getString("ZHSDAT")))
                            + "|" + df.format(openBal)
                            + "|" + mutasi.getString("ZHPSQ") + "|"
                            + "|" + debetcredit
                            + "|" + df.format(cashVal)
                            + "|" + mutasi.getString("ZHNR") + mutasi.getString("ZHNAR2") + mutasi.getString("ZHNAR3") + mutasi.getString("ZHNAR4")
                            + "|" + df.format(closeBal)
                            + "|");
                }
            }
            printWriter.close();
        } catch (IOException | ParseException ex) {
            logger.error(ex.getMessage());

            jsonResponse.put("status", false);

            return jsonResponse;
        }

        jsonResponse.put("status", true);

        return jsonResponse;
    }

    public JSONObject createFileReconBalance(JSONObject request, String localDir) {
        JSONObject jsonResponse = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");

        String valDate = sdf.format(new Date());
        String fileName = "RecBalance_BJB03_" + valDate + "_" + request.getString("SEQ") + ".fsp";

        jsonResponse.put("filename", fileName);

        try {
            FileWriter fileWriter = new FileWriter(localDir + fileName);

            JSONArray data = request.getJSONArray("DATA");

            PrintWriter printWriter = new PrintWriter(fileWriter);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
            String reffNum = sdf2.format(new Date());
            DecimalFormat df = new DecimalFormat("#0.0000");

            int counter = 0;
            for (int i = 0; i < data.length(); i++) {
                BigDecimal balance = new BigDecimal(data.getJSONObject(i).getString("balance"));

                printWriter.println("bjb" + reffNum + String.format("%023d", counter)
                        + "|" + "BJB03"
                        + "|" + data.getJSONObject(i).getString("bankAccountNumber")
                        + "|" + "IDR"
                        + "|" + valDate
                        + "|" + df.format(balance));

                counter++;
            }

            printWriter.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());

            jsonResponse.put("status", false);

            return jsonResponse;
        }

        jsonResponse.put("status", true);

        return jsonResponse;
    }

    public JSONObject createFileReconBalance(List<KseiRdnBalance> request, String localDir) {
        JSONObject jsonResponse = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");

        String valDate = sdf.format(new Date());

        String baseName = "RecBalance_BJB03_" + valDate + "_";
        String extension = "fsp";

        Path filePath = findFileName(localDir, baseName, extension);

        jsonResponse.put("filename", filePath.getFileName().toString());

        try {
            FileWriter fileWriter = new FileWriter(filePath.toFile());

//            JSONArray data = request.getJSONArray("DATA");
            PrintWriter printWriter = new PrintWriter(fileWriter);

            String reffNum = sdf2.format(new Date());
//            final NumberFormat format = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
            DecimalFormat df = new DecimalFormat(decimalFormatStr, symbols);
            df.setParseBigDecimal(true);

            for (int i = 0; i < request.size(); i++) {
                KseiRdnBalance rdnData = request.get(i);
                String balanceStr = rdnData.getBalance().contains("-") ? "0" : rdnData.getBalance().replaceAll(",","");

                logger.info("for account " + rdnData.getRdn());
                logger.info("balance to be format: " + rdnData.getBalance());
//                BigDecimal balance = new BigDecimal(rdnData.getBalance());
                BigDecimal balance = (BigDecimal) df.parse(balanceStr);

                printWriter.println("bjb" + reffNum + String.format("%023d", i)
                        + "|" + "BJB03"
                        + "|" + rdnData.getRdn()
                        + "|" + "IDR"
                        + "|" + valDate
                        + "|" + df.format(balance));
//                        + "|" + df.parse(balanceStr));
            }

            printWriter.close();
        } catch (IOException ex) {
            logger.error("IOException: " + ex.getMessage());

            jsonResponse.put("status", false);

            return jsonResponse;
        } catch (ParseException ex) {
            logger.error("ParseException: " + ex.getMessage());

            jsonResponse.put("status", false);

            return jsonResponse;
        }

        jsonResponse.put("status", true);

        return jsonResponse;
    }

    public JSONObject createFileReconDataStatic(JSONObject request, String localDir) {
        JSONObject jsonResponse = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");

        Instant now = Instant.now();
        Instant yesterday = now.minus(1, ChronoUnit.DAYS);

        String valDate = sdf.format(new Date());
        String prevDate = sdf.format(Date.from(yesterday));
        String fileName = "DataStaticInv_BJB03_" + valDate + "_" + request.getString("SEQ") + ".fsp";

        jsonResponse.put("filename", fileName);

        try {
            FileWriter fileWriter = new FileWriter(localDir + fileName);

            JSONArray data = request.getJSONArray("DATA");

            PrintWriter printWriter = new PrintWriter(fileWriter);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
            String reffNum = sdf2.format(new Date());

            int counter = 0;
            for (int i = 0; i < data.length(); i++) {

                printWriter.println("bjb" + reffNum + String.format("%023d", counter)
                        + "|" + data.getJSONObject(i).getString("participantid").substring(0, 5)
                        + "|" + data.getJSONObject(i).getString("participantname")
                        + "|" + data.getJSONObject(i).getString("investorname")
                        + "|" + data.getJSONObject(i).getString("sidnumber")
                        + "|" + data.getJSONObject(i).getString("accountnumber")
                        + "|" + data.getJSONObject(i).getString("bankaccountnumber")
                        + "|" + "BJB03"
                        + "|" + prevDate
                        + "|" + data.getJSONObject(i).getString("activity").substring(0, 1).toUpperCase());

                counter++;
            }

            printWriter.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());

            jsonResponse.put("status", false);

            return jsonResponse;
        }

        jsonResponse.put("status", true);

        return jsonResponse;
    }

    public JSONObject createFileReconDataStatic2(List<KseiRdnAccountHistory> requestList, Path filePath) {
        JSONObject jsonResponse = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");

        Instant now = Instant.now();
        Instant yesterday = now.minus(1, ChronoUnit.DAYS);
        
        String prevDate = sdf.format(Date.from(yesterday));
        
        jsonResponse.put("filename", filePath.getFileName().toString());

        try {
            FileWriter fileWriter = new FileWriter(filePath.toFile(), true);

            PrintWriter printWriter = new PrintWriter(fileWriter);

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
            String reffNum = sdf2.format(new Date());

            int counter = 0;
            for (int i = 0; i < requestList.size(); i++) {
                printWriter.println("bjb" + reffNum + String.format("%023d", counter)
                        + "|" + requestList.get(i).getPeId()
                        + "|" + requestList.get(i).getPeId()
                        + "|" + requestList.get(i).getName()
                        + "|" + requestList.get(i).getSid()
                        + "|" + requestList.get(i).getSre()
                        + "|" + requestList.get(i).getRdn()
                        + "|" + "BJB03"
                        + "|" + prevDate
                        + "|" + requestList.get(i).getActivity().substring(0, 1).toUpperCase());

                counter++;
            }
            
            printWriter.close();
        } catch (IOException ex) {
            logger.error(ex.getMessage());

            jsonResponse.put("status", false);

            return jsonResponse;
        }
        
        jsonResponse.put("status", true);

        return jsonResponse;
    }

    public static Path findFileName(final String directory, final String baseName, final String extension) {
        for (int i = 1; i < 100; i++) {
            Path ret = Paths.get(directory, String.format("%s%02d.%s", baseName, i, extension));
            if (!Files.exists(ret)) {
                return ret;
            }
        }

        throw new IllegalStateException("Cannot find suitable filename");
    }

    public static String findLastFileName(final String directory, final String baseName, final String extension) {
        String lastFile = "";

        for (int i = 1; i < 100; i++) {
            Path ret = Paths.get(directory, String.format("%s%02d.%s", baseName, i, extension));
            if (!Files.exists(ret)) {
                return lastFile;
            }

            lastFile = ret.getFileName().toString();
        }

        throw new IllegalStateException("Cannot find suitable filename");
    }
}
