/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.controller;

import id.co.bankbjb.rdn.ftp.config.FtpProperties;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author LENOVO
 */
public class FtpClientHandler {

    private static final Logger logger = LoggerFactory.getLogger(FtpClientHandler.class);

//    private static String server = "10.112.6.14";
//    private static String server = "10.6.226.219";
//    private static int port = 21;
//    private static String user = "bjb03";
//    private static String pass = "Bjb@Ksei";
//    private static String user = "rsud";
//    private static String pass = "rsud123!@#";

    public boolean uploadFile(FtpProperties ftpProperties, String localFileFullPathName, String remoteDirectory, String remoteFileNameTarget) {
        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(ftpProperties.getHost(), ftpProperties.getPort());
            ftpClient.login(ftpProperties.getUser(), ftpProperties.getPass());
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            File localFile = new File(localFileFullPathName);

            String remoteFile = remoteDirectory + remoteFileNameTarget;

            boolean done;
            try (InputStream inputStream = new FileInputStream(localFile)) {
                logger.info("Uploading file " + localFile.getName() + " to " + remoteDirectory);
                done = ftpClient.storeFile(remoteFile, inputStream);
            }

            if (!done) {
                logger.error("Failed to upload file" + localFile.getName());

                return false;
            }

            logger.info("Upload file success");
        } catch (IOException ex) {
            logger.error(ex.getMessage());

            return false;
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage());

                return false;
            }
        }

        return true;
    }

    public boolean downloadFile(FtpProperties ftpProperties, String remoteDirectory, String localFileFullPath) {
        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(ftpProperties.getHost(), ftpProperties.getPort());
            ftpClient.enterLocalPassiveMode();

            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                logger.error("Connect failed");

                return false;
            }

            boolean isLogin = ftpClient.login(ftpProperties.getUser(), ftpProperties.getPass());
            if (!isLogin) {
                logger.error("Could not login to the server");

                return false;
            }

            FTPFile[] ftpFiles = ftpClient.listFiles(remoteDirectory);
            
            if (ftpFiles != null && ftpFiles.length > 0) {
                for (FTPFile aFile : ftpFiles) {
                    String currentFileName = aFile.getName();
                    if (currentFileName.equals(".")
                            || currentFileName.equals("..")) {
                        // skip parent directory and directory itself
                        continue;
                    }

                    if (!aFile.isDirectory()) {
                        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

                        String remoteFile = remoteDirectory + currentFileName;
                        File downloadFile = new File(localFileFullPath + currentFileName);

                        boolean success;
                        try (OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile))) {
                            success = ftpClient.retrieveFile(remoteFile, outputStream1);
                        }

                        if (!success) {
                            logger.error("Failed to download file " + remoteFile);
                        } else {
                            ftpClient.deleteFile(remoteFile);
                            
                            //open zip files here
                            ZipFile zipFile = new ZipFile(downloadFile);

                            for (Enumeration e = zipFile.entries(); e.hasMoreElements();) {
                                ZipEntry entry = (ZipEntry) e.nextElement();
                                if (!entry.isDirectory()) {
                                    if (FilenameUtils.getExtension(entry.getName()).equals("fsp")) {
                                        logger.info("READING FILE " + entry.getName() + " IN " + downloadFile.getName());
                                        
                                        getTxtFileContents(zipFile.getInputStream(entry));
                                        //do your thing
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                logger.error("File not found");

                return false;
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage());

            return false;
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage());

                return false;
            }
        }

        return true;
    }

    private void listDirectory(FTPClient ftpClient, String parentDir, String currentDir, int level) throws IOException {
        String dirToList = parentDir;

        if (!currentDir.equals("")) {
            dirToList += "/" + currentDir;
        }

        FTPFile[] subFiles = ftpClient.listFiles(dirToList);
        if (subFiles != null && subFiles.length > 0) {
            for (FTPFile aFile : subFiles) {
                String currentFileName = aFile.getName();
                if (currentFileName.equals(".")
                        || currentFileName.equals("..")) {
                    // skip parent directory and directory itself
                    continue;
                }
                for (int i = 0; i < level; i++) {
                    System.out.print("\t");
                }
                if (aFile.isDirectory()) {
                    System.out.println("[" + currentFileName + "]");
                    listDirectory(ftpClient, dirToList, currentFileName, level + 1);
                } else {
                    System.out.println(currentFileName);
                }
            }
        }
    }

    private static void getTxtFileContents(InputStream in) {
        StringBuilder out = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;

        logger.info("---------------------------------");
        logger.info("Reading file response contents...");
        logger.info("---------------------------------");
        try {
            while ((line = reader.readLine()) != null) {
                out.append(line);

                logger.info(line);
            }

            logger.info("---------------------------------");
            logger.info("End of contents");
            logger.info("---------------------------------");
        } catch (IOException e) {
            // do something, probably not a text file
            logger.error(e.getMessage());
        }
    }
}
