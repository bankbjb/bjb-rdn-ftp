/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author LENOVO
 */
@Entity
@Table(name = "ksei_rdn_account_history")
public class KseiRdnAccountHistory {
    
    @Id
    @Column(name = "reff_id")
    private String reffId;
    
    @Column(name = "pe_id")
    private String peId;
    
    @Column(name = "sid")
    private String sid;
    
    @Column(name = "sre")
    private String sre;
    
    @Column(name = "rdn")
    private String rdn;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "activity")
    private String activity;
    
    @Column(name = "activity_date")
    private Date activityDate;

    public KseiRdnAccountHistory() {
    }

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public String getPeId() {
        return peId;
    }

    public void setPeId(String peId) {
        this.peId = peId;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSre() {
        return sre;
    }

    public void setSre(String sre) {
        this.sre = sre;
    }

    public String getRdn() {
        return rdn;
    }

    public void setRdn(String rdn) {
        this.rdn = rdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

}
