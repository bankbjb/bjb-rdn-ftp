/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.repository;

import id.co.bankbjb.rdn.ftp.model.RdnInformasiNasabah;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author LENOVO
 */
@Repository
public interface RdnInformasiNasabahRepository extends JpaRepository<RdnInformasiNasabah, UUID> 
{
    //https://docs.spring.io/spring-data/jpa/docs/1.5.0.RELEASE/reference/html/jpa.repositories.html
//    List<RdnInformasiNasabah> findAll();
    @Query("SELECT DISTINCT i.norek FROM RdnInformasiNasabah i WHERE i.norek IS NOT NULL")
    public List<String> findDistinctNorekByNorekIsNotNull();
    
    public List<RdnInformasiNasabah> findByNorekIsNotNull();
    
//    @Query("SELECT DISTINCT i.norek FROM RdnInformasiNasabah i WHERE i.norek IS NOT NULL AND i.norek != ''")
//    public List<RdnInformasiNasabah> findByNorekIsNotNullAndNotEmpty();
}
