/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.controller;

import id.co.bankbjb.rdn.ftp.config.FtpProperties;
import id.co.bankbjb.rdn.ftp.config.GssProperties;
import id.co.bankbjb.rdn.ftp.model.KseiRdnBalance;
import id.co.bankbjb.rdn.ftp.model.RdnInformasiNasabah;
import id.co.bankbjb.rdn.ftp.repository.KseiRdnBalanceRepository;
import id.co.bankbjb.rdn.ftp.repository.RdnInformasiNasabahRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.commons.collections4.ListUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
@RestController
@RequestMapping("/rdn/ftp")
public class FtpController {

    private static final Logger logger = LoggerFactory.getLogger(FtpController.class);

    @Autowired
    private FtpProperties properties;
    @Autowired
    private GssProperties gssProperties;
    @Autowired
    private RdnInformasiNasabahRepository rdnInformasiNasabahRepo;
    @Autowired
    private KseiRdnBalanceRepository kseiRdnBalanceRepo;

    private final String uri_dev = "http://192.168.226.137:9008/GSS_Message";

    @PostMapping(path = "/account-statement", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String sendReconActStmnt(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String localDir = "ftp/Outgoing/BJB03ReconActStmnt/";
        JSONObject jsonFile = new FileHandler().createFileReconActStmnt(response, localDir);

        if (!jsonFile.getBoolean("status")) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to create file");

            response.put("RC", responseRC);

            logger.error("Failed to create file " + jsonFile.getString("filename"));

            return response.toString();
        }

        String localFile = localDir + jsonFile.getString("filename");
        String remoteDir = "BJB03ReconActStmnt/";
        String remoteFile = jsonFile.getString("filename");

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to upload file");

            response.put("RC", responseRC);

            logger.error("Failed to upload file " + jsonFile.getString("filename"));

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/balance", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String sendReconBalance(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String localDir = "ftp/Outgoing/BJB03ReconBalance/";
        JSONObject jsonFile = new FileHandler().createFileReconBalance(response, localDir);

        if (!jsonFile.getBoolean("status")) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to create file");

            response.put("RC", responseRC);

            logger.error("Failed to create file " + jsonFile.getString("filename"));

            return response.toString();
        }

        String localFile = localDir + jsonFile.getString("filename");
        String remoteDir = "BJB03ReconBalance/";
        String remoteFile = jsonFile.getString("filename");

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to upload file");

            response.put("RC", responseRC);

            logger.error("Failed to upload file " + jsonFile.getString("filename"));

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/data-static", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String sendReconDataStatic(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String localDir = "ftp/Outgoing/BJB03ReconDataStatic/";
        JSONObject jsonFile = new FileHandler().createFileReconDataStatic(response, localDir);

        if (!jsonFile.getBoolean("status")) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to create file");

            response.put("RC", responseRC);

            logger.error("Failed to create file " + jsonFile.getString("filename"));

            return response.toString();
        }

        String localFile = localDir + jsonFile.getString("filename");
        String remoteDir = "BJB03ReconDataStatic/";
        String remoteFile = jsonFile.getString("filename");

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to upload file");

            response.put("RC", responseRC);

            logger.error("Failed to upload file " + jsonFile.getString("filename"));

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/account-statement-result", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String getReconActStmntResult(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String remoteDirectory = "BJB03ReconActStmntResult/";
        String localFileFullPath = "ftp/Incoming/BJB03ReconActStmntResult/";

        boolean downloadFile = new FtpClientHandler().downloadFile(properties, remoteDirectory, localFileFullPath);

        if (!downloadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to download file");

            response.put("RC", responseRC);

            logger.error("Failed to download file from " + remoteDirectory);

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/balance-result", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String getReconBalanceResult(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String remoteDirectory = "BJB03ReconBalanceResult/";
        String localFileFullPath = "ftp/Incoming/BJB03ReconBalanceResult/";

        boolean downloadFile = new FtpClientHandler().downloadFile(properties, remoteDirectory, localFileFullPath);

        if (!downloadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to download file");

            response.put("RC", responseRC);

            logger.error("Failed to download file from " + remoteDirectory);

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/data-static-result", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String getReconDataStaticResult(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String remoteDirectory = "BJB03ReconDataStaticResult/";
        String localFileFullPath = "ftp/Incoming/BJB03ReconDataStaticResult/";

        boolean downloadFile = new FtpClientHandler().downloadFile(properties, remoteDirectory, localFileFullPath);

        if (!downloadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to download file");

            response.put("RC", responseRC);

            logger.error("Failed to download file from " + remoteDirectory);

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @RequestMapping("/rdn-info-nasabah")
    public List<RdnInformasiNasabah> getallRdnInformasiNasabah() {
        List<RdnInformasiNasabah> listInfoNasabah = rdnInformasiNasabahRepo.findAll();

        logger.info("info_nasabah size: " + listInfoNasabah.size());

        return listInfoNasabah;
    }

    @RequestMapping("/list-account")
    public List<String> getListAccount() {
        List<String> listInfoNasabah = rdnInformasiNasabahRepo.findDistinctNorekByNorekIsNotNull();

        logger.info("account size: " + listInfoNasabah.size());

        return listInfoNasabah;
    }

    @RequestMapping("/list-account-balance")
    public String getListAccountBalance() throws InterruptedException {
        JSONArray result = new JSONArray();
        List<String> listAccount = rdnInformasiNasabahRepo.findDistinctNorekByNorekIsNotNull();

        logger.info("account size: " + listAccount.size());

        JSONObject gssRequest = new JSONObject();
        gssRequest.put("PCC", "5");
        gssRequest.put("CC", "0001");
        gssRequest.put("PC", "001001");
        gssRequest.put("MC", "90023");
        gssRequest.put("MT", "9200");
        gssRequest.put("FC", "AB");
        gssRequest.put("REMOTEIP", "127.0.0.1");
        gssRequest.put("SID", "singleUserIDWebTeller");
        gssRequest.put("CID", "TG-CB-00-0000089");

        logger.info("==START UPDATE ACCOUNT BALANCE FOR " + listAccount.size() + " ACCOUNT==");

        List<List<String>> partitionList = ListUtils.partition(listAccount, 1000);
        logger.info("==PARTITIONED SIZE = " + partitionList.size());
        for (List<String> list : partitionList) {
            logger.info("==STARTING BATCH UPDATE FOR " + list.size() + " RECORDS==");

            for (int i = 0; i < list.size(); i++) {
                String account = list.get(i);

                JSONObject mpi = new JSONObject();
                mpi.put("ZLEAN", account);

                gssRequest.put("MPI", mpi);
                gssRequest.put("DT", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
                gssRequest.put("ST", new SimpleDateFormat("HHmmssSSS").format(new Date()));

                JSONObject gssResponse = sendGssApiRequest(gssRequest);
                JSONObject resultElement = new JSONObject();
                resultElement.put("ACC", account);

                //use this if id is generatedValue
/*
            KseiRdnBalance kseiRdnBalance;
            if (kseiRdnBalanceRepo.findById(account).isPresent()) {
                kseiRdnBalance = kseiRdnBalanceRepo.findById(account).get();
            } else {
                kseiRdnBalance = new KseiRdnBalance();
                kseiRdnBalance.setRdn(account);
                kseiRdnBalance.setBalance("0");
            }
                 */
                KseiRdnBalance kseiRdnBalance = new KseiRdnBalance();
                kseiRdnBalance.setRdn(account);
                kseiRdnBalance.setBalance("0");

                if (gssResponse.getString("RC").equals("0000")) {
                    resultElement.put("BAL", gssResponse.getJSONObject("MPO").getJSONObject("0").getString("ZLBAL"));

                    kseiRdnBalance.setBalance(gssResponse.getJSONObject("MPO").getJSONObject("0").getString("ZLBAL"));
//                kseiRdnBalance.setBalance(new SimpleDateFormat("HHmmssSSS").format(new Date()));
                } else {
                    resultElement.put("BAL", "0");

//                kseiRdnBalance.setBalance("0");
                }

                result.put(resultElement);

                kseiRdnBalanceRepo.save(kseiRdnBalance);
            }

            logger.info("==WAITING 5 MINUTES BEFORE NEXT BATCH");
            TimeUnit.MINUTES.sleep(5);
        }

        logger.info("==FINISH UPDATE ACCOUNT BALANCE==");

        return result.toString();
    }

    private JSONObject sendGssSocketRequest(JSONObject request) {
        JSONObject jsonResult = new JSONObject();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        try {
            logger.info("REQ-GSS-SOCK: " + request);

            HttpEntity<String> requestStr = new HttpEntity<>(request.toString(), headers);

//            String result = restTemplate.postForObject(uri_dev, requestStr, String.class);
            String result = restTemplate.postForObject(gssProperties.getUrl(), requestStr, String.class);

            logger.info("RSP-GSS-SOCK: " + result);

            return new JSONObject(result);
        } catch (RestClientException ex) {
            logger.error("ERROR: " + ex.getMessage());
        } catch (Exception ex) {
            logger.error("UNKNOWN_ERROR: " + ex.getMessage());
        }

        return jsonResult;
    }
    
    private JSONObject sendGssApiRequest(JSONObject request) {
        JSONObject jsonResult = new JSONObject();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        try {
            logger.info("REQ-GSS: " + "[" + gssProperties.getUrl() + "] " + request);

            HttpEntity<String> requestStr = new HttpEntity<>(request.toString(), headers);

//            String result = restTemplate.postForObject(uri_dev, requestStr, String.class);
            String result = restTemplate.postForObject(gssProperties.getUrl(), requestStr, String.class);

            logger.info("RSP-GSS: " + result);

            return new JSONObject(result);
        } catch (RestClientException ex) {
            logger.error("ERROR: " + ex.getMessage());
        } catch (Exception ex) {
            logger.error("UNKNOWN_ERROR: " + ex.getMessage());
        }

        return jsonResult;
    }

    @RequestMapping("/create-file-balance")
    public List<KseiRdnBalance> createFileBalance() {
        List<KseiRdnBalance> listAccountBalance = kseiRdnBalanceRepo.findByJoinRdnInformasiNasabahOnNorek();

        logger.info("balance size: " + listAccountBalance.size());

//        JSONObject response = new JSONObject(requestBody);
        String localDir = "ftp/Outgoing/BJB03ReconBalance/";
        JSONObject jsonFile = new FileHandler().createFileReconBalance(listAccountBalance, localDir);

        logger.info("created file: " + jsonFile);
        try {
            TimeUnit.MINUTES.sleep(5);

            logger.info("TimeUnit wait for 5 minutes is finished");
        } catch (InterruptedException ex) {
            logger.error("failed to wait for 5 minutes, caused by: " + ex.getMessage());
        }

        return listAccountBalance;
    }

    @RequestMapping("/collect-balance")
    public List<RdnInformasiNasabah> collectBalance() {
//        List<RdnInformasiNasabah> listBalance = rdnInformasiNasabahRepo.findAll();
        List<RdnInformasiNasabah> listBalance = rdnInformasiNasabahRepo.findByNorekIsNotNull();

        logger.info("collected size: " + listBalance.size());
//        JSONObject response = new JSONObject(requestBody);
//        
//        String localDir = "ftp/Outgoing/BJB03ReconBalance/";
//        JSONObject jsonFile = new FileHandler().createFileReconBalance(response, localDir);

        return listBalance;
    }

    @PostMapping(path = "/retry-recon-balance", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String retrySendReconBalance(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String localDir = "ftp/Outgoing/BJB03ReconBalance/";
        String localFile = localDir + response.getString("filename");

        String remoteDir = "BJB03ReconBalance/";
        String remoteFile = response.getString("filename");

        logger.info("upload file " + remoteFile + " to directory " + remoteDir);

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to upload file");

            response.put("RC", responseRC);

            logger.error("Failed to upload file " + response.getString("filename"));

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/retry-recon-data-static", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String retrySendReconDataStatic(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String localDir = "ftp/Outgoing/BJB03ReconDataStatic/";
        String localFile = localDir + response.getString("filename");

        String remoteDir = "BJB03ReconDataStatic/";
        String remoteFile = response.getString("filename");

        logger.info("upload file " + remoteFile + " to directory " + remoteDir);

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to upload file");

            response.put("RC", responseRC);

            logger.error("Failed to upload file " + response.getString("filename"));

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }

    @PostMapping(path = "/retry-recon-account-statement", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String retrySendReconAccountStatement(@RequestBody String requestBody) {
        JSONObject response = new JSONObject(requestBody);
        JSONObject responseRC = new JSONObject();

        String localDir = "ftp/Outgoing/BJB03ReconActStmnt/";
        String localFile = localDir + response.getString("filename");

        String remoteDir = "BJB03ReconActStmnt/";
        String remoteFile = response.getString("filename");

        logger.info("upload file " + remoteFile + " to directory " + remoteDir);

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            responseRC.put("RC", "RDN-99");
            responseRC.put("RCMSG", "ERROR: Failed to upload file");

            response.put("RC", responseRC);

            logger.error("Failed to upload file " + response.getString("filename"));

            return response.toString();
        }

        responseRC.put("RC", "RDN-00");
        responseRC.put("RCMSG", "SUCCESS");

        response.put("RC", responseRC);

        return response.toString();
    }
}
