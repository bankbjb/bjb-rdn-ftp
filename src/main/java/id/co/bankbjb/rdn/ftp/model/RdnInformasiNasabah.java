/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Type;

/**
 *
 * @author LENOVO
 */
@Entity
@Table(name = "rdn_informasi_nasabah")
public class RdnInformasiNasabah {
    
    @Type(type="org.hibernate.type.PostgresUUIDType")
    @Id
    @Column(name = "nasabah_id")
    @JsonIgnore
    private UUID nasabahId;
    
    @Column(name = "cif")
    private String cif;
    
    @Column(name = "norek")
    private String norek;
    
    @Column(name = "nama_lengkap")
    private String namaLengkap;

//    @NotNull
//    @OneToOne(fetch = FetchType.LAZY)
//    @OneToOne(optional = false)
//    @JoinColumn(name = "norek", referencedColumnName = "rdn")
//    @JoinColumn(name = "norek", updatable = false, insertable = false)
//    private KseiRdnBalance norek;
    
    public RdnInformasiNasabah() {
    }

//    public RdnInformasiNasabah(UUID nasabahId, String cif, String suffix, String norek) {
//        this.nasabahId = nasabahId;
//        this.cif = cif;
//        this.suffix = suffix;
//        this.norek = norek;
//    }

    public UUID getNasabahId() {
        return nasabahId;
    }

    public void setNasabahId(UUID nasabahId) {
        this.nasabahId = nasabahId;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

//    public KseiRdnBalance getNorek() {
//        return norek;
//    }

//    public void setNorek(KseiRdnBalance norek) {
//        this.norek = norek;
//    }
    
}
