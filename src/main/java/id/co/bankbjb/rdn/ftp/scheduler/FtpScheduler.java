/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.scheduler;

import id.co.bankbjb.rdn.ftp.config.FtpProperties;
import id.co.bankbjb.rdn.ftp.config.GssProperties;
import id.co.bankbjb.rdn.ftp.controller.FileHandler;
import id.co.bankbjb.rdn.ftp.controller.FtpClientHandler;
import id.co.bankbjb.rdn.ftp.model.KseiRdnAccountHistory;
import id.co.bankbjb.rdn.ftp.model.KseiRdnBalance;
import id.co.bankbjb.rdn.ftp.repository.KseiRdnAccountHistoryRepository;
import id.co.bankbjb.rdn.ftp.repository.KseiRdnBalanceRepository;
import id.co.bankbjb.rdn.ftp.repository.RdnInformasiNasabahRepository;
import java.io.File;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author LENOVO
 */
@Component
public class FtpScheduler {

    private static final Logger logger = LoggerFactory.getLogger(FtpScheduler.class);

    @Autowired
    private FtpProperties properties;
    @Autowired
    private GssProperties gssProperties;
    @Autowired
    private RdnInformasiNasabahRepository rdnInformasiNasabahRepo;
    @Autowired
    private KseiRdnBalanceRepository kseiRdnBalanceRepo;
    @Autowired
    private KseiRdnAccountHistoryRepository accountHistoryRepo;

    //cron expression: <second> <minute> <hour> <day-of-month> <month> <day-of-week> <year>
    //execute the task every minute starting at 9:00 AM and ending at 9:59 AM, every day
    //@Scheduled(cron = "0 * 9 * * ?")
    @Scheduled(cron = "0 0 3 * * ?")
    public void testRunCronJob() {
        logger.info("INFO: Test scheduler at 3:00 every days.");
    }

    @Scheduled(cron = "0 15 0 * * ?")
    public void createAndSendDataStaticFileCronJob() {
        logger.info("###Scheduler at 00:15 every days. ACTION: create and send file recon-data-static");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);

        Date yesterday = DateUtils.addDays(new Date(), -1);

        List<KseiRdnAccountHistory> listAccountHistory = accountHistoryRepo.findYesterdayHistory(yesterday);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String valDate = sdf.format(new Date());

        String localDir = "ftp/Outgoing/BJB03ReconDataStatic/";
        String baseName = "DataStaticInv_BJB03_" + valDate + "_";
        String extension = "fsp";

        Path filePath = FileHandler.findFileName(localDir, baseName, extension);

        JSONObject jsonFile = new FileHandler().createFileReconDataStatic2(listAccountHistory, filePath);

        if (!jsonFile.getBoolean("status")) {
            logger.error("Failed to create file " + jsonFile.getString("filename"));
        }

        logger.info("===FINISH CREATE TRANSACTION HISTORY FILE FOR " + listAccountHistory.size() + " ACCOUNTS===");

        String localFile = localDir + jsonFile.getString("filename");
        String remoteDir = "BJB03ReconDataStatic/";
        String remoteFile = jsonFile.getString("filename");

        logger.info("upload file " + remoteFile + " to directory " + remoteDir);

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            logger.error("Failed to upload file: " + jsonFile.getString("filename"));

            return;
        }

        logger.info("===File " + jsonFile.getString("filename") + " uploaded succesfully===");

        try {
            TimeUnit.MINUTES.sleep(5);

            fetchReconDataStaticResult();
        } catch (InterruptedException ex) {
            logger.error("Failed to start file downloader: " + ex.getMessage());
        }
    }

    @Scheduled(cron = "0 0 19 * * ?")
//    @Scheduled(cron = "0 30 14 * * ?")
    public void createMutationFileCronJob() {
        logger.info("###Scheduler at 19:00 every days. ACTION: retrieve transaction history through GSS");

        List<String> listAccount = rdnInformasiNasabahRepo.findDistinctNorekByNorekIsNotNull();

        //GSS TH Request Template
        JSONObject gssRequest = new JSONObject();
        gssRequest.put("PCC", "5");
        gssRequest.put("CC", "0001");
        gssRequest.put("PC", "001001");
        gssRequest.put("MC", "90023");
        gssRequest.put("MT", "9200");
        gssRequest.put("FC", "TH");
        gssRequest.put("REMOTEIP", "127.0.0.1");
        gssRequest.put("SID", "singleUserIDWebTeller");
        gssRequest.put("CID", "TG-CB-00-0000089");

        logger.info("===START FETCHING TRANSACTION HISTORY FOR " + listAccount.size() + " ACCOUNTS===");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String valDate = sdf.format(DateUtils.addDays(new Date(), 1));

        String localDir = "ftp/Outgoing/BJB03ReconActStmnt/";
        String baseName = "RecActStmt_BJB03_" + valDate + "_";
        String extension = "fsp";

        Path filePath = FileHandler.findFileName(localDir, baseName, extension);

        for (int i = 0; i < listAccount.size(); i++) {
            String account = listAccount.get(i);

            JSONObject mpi = new JSONObject();
            mpi.put("ZLEAN", account);
            mpi.put("ZLVFRZ", new SimpleDateFormat("ddMMyy").format(new Date()));
            mpi.put("ZLVTOZ", new SimpleDateFormat("ddMMyy").format(new Date()));
            mpi.put("PGNUM", "1");
            mpi.put("PGSIZE", "12");

            gssRequest.put("MPI", mpi);
            gssRequest.put("DT", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            gssRequest.put("ST", new SimpleDateFormat("HHmmssSSS").format(new Date()));

            JSONObject gssResponse = sendGssRequest(gssRequest);

            JSONObject jsonFile = new FileHandler().createFileReconActStmnt2(gssResponse, filePath);

            if (!jsonFile.getBoolean("status")) {
                logger.error("Failed to create file " + jsonFile.getString("filename") + "for account: " + account);

//                continue;
            }
        }

        logger.info("===FINISH CREATE TRANSACTION HISTORY FILE FOR " + listAccount.size() + " ACCOUNTS===");
    }

    @Scheduled(cron = "0 0 18 * * ?")
    public void getAllBalanceCronJob() {
        logger.info("###Scheduler at 18:00 every days. ACTION: retrieve account balance through GSS");

        List<String> listAccount = rdnInformasiNasabahRepo.findDistinctNorekByNorekIsNotNull();

        //GSS AB Request Template
        JSONObject gssRequest = new JSONObject();
        gssRequest.put("PCC", "5");
        gssRequest.put("CC", "0001");
        gssRequest.put("PC", "001001");
        gssRequest.put("MC", "90023");
        gssRequest.put("MT", "9200");
        gssRequest.put("FC", "AB");
        gssRequest.put("REMOTEIP", "127.0.0.1");
        gssRequest.put("SID", "singleUserIDWebTeller");
        gssRequest.put("CID", "TG-CB-00-0000089");

        logger.info("===START FETCHING ACCOUNT BALANCE FOR " + listAccount.size() + " ACCOUNTS===");

        for (int i = 0; i < listAccount.size(); i++) {
            try {
                if ((i + 1) % 50 == 0) {
                    Thread.sleep(10000);
                }
            } catch (InterruptedException ex) {
                logger.error("Error set interval fetching balance " + ex.getMessage());
            }

            String account = listAccount.get(i);

            JSONObject mpi = new JSONObject();
            mpi.put("ZLEAN", account);

            gssRequest.put("MPI", mpi);
            gssRequest.put("DT", new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            gssRequest.put("ST", new SimpleDateFormat("HHmmssSSS").format(new Date()));

            JSONObject gssResponse = sendGssRequest(gssRequest);

            Optional<KseiRdnBalance> rdnBalance = kseiRdnBalanceRepo.findById(account);
            KseiRdnBalance kseiRdnBalance = new KseiRdnBalance();

            if (rdnBalance.isPresent()) {
                kseiRdnBalance = rdnBalance.get();
            } else {
                kseiRdnBalance.setRdn(account);
                kseiRdnBalance.setBalance("0");
            }

            if (gssResponse.getString("RC").equals("0000")) {
                kseiRdnBalance.setBalance(gssResponse.getJSONObject("MPO").getJSONObject("0").getString("ZLBAL"));
            } else {
                logger.error("Failed fetching balance for account " + account);
            }

            kseiRdnBalanceRepo.save(kseiRdnBalance);
        }

        logger.info("===FINISH UPDATE ACCOUNT BALANCE===");
        logger.info("###End of scheduler at 18:00 every days. ACTION: retrieve account balance through GSS");
    }

    @Scheduled(cron = "0 30 0 * * ?")
    public void sendAndFetchReconBalanceCronJob() {
        logger.info("###Scheduler at 00:30 every days. ACTION: send reconAccountBalance file");

        List<KseiRdnBalance> listAccountBalance = kseiRdnBalanceRepo.findByJoinRdnInformasiNasabahOnNorek();
        logger.info("fetched records size: " + listAccountBalance.size());

        String localDir = "ftp/Outgoing/BJB03ReconBalance/";
        JSONObject jsonFile = new FileHandler().createFileReconBalance(listAccountBalance, localDir);
        logger.info("status create file: " + jsonFile);

        if (!jsonFile.getBoolean("status")) {
            logger.error("Failed to create file: " + jsonFile.getString("filename"));

            return;
        }

        String localFile = localDir + jsonFile.getString("filename");
        String remoteDir = "BJB03ReconBalance/";
        String remoteFile = jsonFile.getString("filename");

        logger.info("upload file " + remoteFile + " to directory " + remoteDir);

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            logger.error("Failed to upload file: " + jsonFile.getString("filename"));

            return;
        }

        logger.info("###End of scheduler at 00:15 every days. ACTION: send reconAccountBalance file");

        try {
//            Thread.sleep(5 * 60000);
            TimeUnit.MINUTES.sleep(5);

            fetchReconBalanceResult();
        } catch (InterruptedException ex) {
            logger.error("Failed to start file downloader: " + ex.getMessage());
        }
    }

    @Scheduled(cron = "0 30 0 * * ?")
    public void sendAndFetchReconAcctStmtCronJob() {
        logger.info("###Scheduler at 00:30 every days. ACTION: send reconAccountStatement file");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String valDate = sdf.format(new Date());

        String localDir = "ftp/Outgoing/BJB03ReconActStmnt/";
        String baseName = "RecActStmt_BJB03_" + valDate + "_";
        String extension = "fsp";

        String fileName = FileHandler.findLastFileName(localDir, baseName, extension);

        if (fileName.isEmpty()) {
            logger.error("File not found: " + fileName);

            return;
        }

        String localFile = localDir + fileName;
        String remoteDir = "BJB03ReconActStmnt/";
        String remoteFile = fileName;

        File file = new File(localFile);
        if (file.length() == 0) {
            logger.error("File is empty: " + fileName);

            return;
        }

        logger.info("upload file " + remoteFile + " to directory " + remoteDir);

        boolean uploadFile = new FtpClientHandler().uploadFile(properties, localFile, remoteDir, remoteFile);

        if (!uploadFile) {
            logger.error("Failed to upload file: " + fileName);

            return;
        }

        logger.info("###End of scheduler at 00:15 every days. ACTION: send reconAccountBalance file");

        try {
//            Thread.sleep(5 * 60000);
            TimeUnit.MINUTES.sleep(5);

            fetchReconAcctStmtResult();
        } catch (InterruptedException ex) {
            logger.error("Failed to start file downloader: " + ex.getMessage());
        }
    }

    private JSONObject sendGssRequest(JSONObject request) {
        JSONObject jsonResult = new JSONObject();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        try {
            logger.info("REQ-GSS: " + request);

            HttpEntity<String> requestStr = new HttpEntity<>(request.toString(), headers);

            String result = restTemplate.postForObject(gssProperties.getUrl(), requestStr, String.class);

            logger.info("RSP-GSS: " + result);

            return new JSONObject(result);
        } catch (RestClientException ex) {
            logger.error("ERROR: " + ex.getMessage());
        } catch (Exception ex) {
            logger.error("UNKNOWN_ERROR: " + ex.getMessage());
        }

        return jsonResult;
    }

    private void fetchReconDataStaticResult() {
        String remoteDirectory = "BJB03ReconDataStaticResult/";
        String localFileFullPath = "ftp/Incoming/BJB03ReconDataStaticResult/";

        logger.info("trying to download response file at: " + remoteDirectory);

        boolean downloadFile = new FtpClientHandler().downloadFile(properties, remoteDirectory, localFileFullPath);

        if (!downloadFile) {
            logger.error("Failed to download file from " + remoteDirectory);

            return;
        }

        logger.info("success download response file from: " + remoteDirectory);
    }

    private void fetchReconBalanceResult() {
        String remoteDirectory = "BJB03ReconBalanceResult/";
        String localFileFullPath = "ftp/Incoming/BJB03ReconBalanceResult/";

        logger.info("trying to download response file at: " + remoteDirectory);

        boolean downloadFile = new FtpClientHandler().downloadFile(properties, remoteDirectory, localFileFullPath);

        if (!downloadFile) {
            logger.error("Failed to download file from " + remoteDirectory);

            return;
        }

        logger.info("success download response file from: " + remoteDirectory);
    }

    private void fetchReconAcctStmtResult() {
        String remoteDirectory = "BJB03ReconActStmntResult/";
        String localFileFullPath = "ftp/Incoming/BJB03ReconActStmntResult/";

        logger.info("trying to download response file at: " + remoteDirectory);

        boolean downloadFile = new FtpClientHandler().downloadFile(properties, remoteDirectory, localFileFullPath);

        if (!downloadFile) {
            logger.error("Failed to download file from " + remoteDirectory);

            return;
        }

        logger.info("success download response file from: " + remoteDirectory);
    }
}
