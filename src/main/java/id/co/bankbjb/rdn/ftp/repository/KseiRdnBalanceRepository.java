/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.repository;

import id.co.bankbjb.rdn.ftp.model.KseiRdnBalance;
import id.co.bankbjb.rdn.ftp.model.RdnInformasiNasabah;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author LENOVO
 */
@Repository
public interface KseiRdnBalanceRepository extends JpaRepository<KseiRdnBalance, String> {
    
//    @PersistenceContext
//    private EntityManager entityManager;
    
//    public void insertWithEntityManager(KseiRdnBalance kseiRdnBalance) {
//        this.entityManager.persist(kseiRdnBalance);
//    }
    
    @Query("SELECT a FROM KseiRdnBalance a JOIN RdnInformasiNasabah i ON i.norek=a.rdn")
    public List<KseiRdnBalance> findByJoinRdnInformasiNasabahOnNorek();
}
