/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.bankbjb.rdn.ftp.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author LENOVO
 */
@Entity
@Table(name = "ksei_rdn_balance")
public class KseiRdnBalance {
    
    @Id
    @Column(name = "rdn")
    private String rdn;
    
    @Column(name = "balance")
    private String balance;

//    @Id
//    @OneToOne(optional = false)
//    @JoinColumn(name = "rdn", referencedColumnName = "norek", updatable = false, insertable = false)
//    private RdnInformasiNasabah infoNasabah;
    
    public KseiRdnBalance() {
    }

    public String getRdn() {
        return rdn;
    }

    public void setRdn(String rdn) {
        this.rdn = rdn;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

//    public RdnInformasiNasabah getInfoNasabah() {
//        return infoNasabah;
//    }

//    public void setInfoNasabah(RdnInformasiNasabah infoNasabah) {
//        this.infoNasabah = infoNasabah;
//    }

}
